import {myService} from "./service/MyService.js";

const ID_RESULT_CONTAINER_DIV = 'result'

window.onload = () => {
    "use strict";

    const resetResultResponse = () => {
        if(document.getElementById(ID_RESULT_CONTAINER_DIV).childElementCount > 0){
            document.getElementById(ID_RESULT_CONTAINER_DIV).innerHTML = ''
        }
    }

    const addLoading = () => {
        const loadingElement = document.createElement('div')
        loadingElement.innerHTML = "Loading..."
        loadingElement.setAttribute('id', 'loading')
        document.getElementById(ID_RESULT_CONTAINER_DIV).appendChild(loadingElement)
    }

    const removeLoading = () => {
        const loadingElement = document.getElementById('loading')
        loadingElement.remove()
    }

    const handlerServiceResponse = () => {
        resetResultResponse()
        addLoading()
        myService.execute().then((posts) => {
            removeLoading()
            const listElement = document.createElement('ul')
            listElement.setAttribute('id', 'list-result')
            document.getElementById(ID_RESULT_CONTAINER_DIV).appendChild(listElement)
            posts.forEach((post) => {
                const postUiElement = post.createUiElement('li')
                document.getElementById('list-result').appendChild(postUiElement)
            })
        })
    }

    // Install service worker
    if('serviceWorker' in navigator) {
        navigator.serviceWorker.register('worker.js')
            .then(sw => console.log('service worker registered', sw))
            .catch(err => console.error('Error during registering the service worker', err))
    }

    const btnRunService = document.getElementById('btn-run-service')
    btnRunService.addEventListener('click',handlerServiceResponse)

}
