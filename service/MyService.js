export class Post {
    constructor(body, id, title, userId) {
        this.body = body
        this.id = id
        this.title = title
        this.userId = userId
    }

    createUiElement(type = 'div') {
       const postUi = document.createElement(type)
        const id = document.createElement('span')
        id.innerHTML = this.id + ' '
        postUi.appendChild(id)
        const title = document.createElement('span')
        title.innerHTML = 'title :'+this.title
        postUi.appendChild(title)
        postUi.setAttribute('id', this.id)
        return postUi
    }
}

export default class MyService {
    async execute() {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts')
        const posts = await response.json()
        return posts.map(({body, id, title, userId}) => new Post(body, id, title, userId))
    }
}

const myService = new MyService()

export {
    myService
}
