//https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers

const VERSION = "v0.0.4"
let numberCall = 0

const addResourcesToCache = async (resources) => {
    const cache = await caches.open(VERSION);
    await cache.addAll(resources);
};

self.addEventListener('install', event => {
    console.log('Service Worker installing...')
    event.waitUntil(
        addResourcesToCache([
                //'.index.html'
            ])
    )
})

self.addEventListener('activate', e => {
    console.log('Service Worker activating')
})

const putInCache = async (request, response) => {
    const cache = await caches.open(VERSION);
    await cache.put(request, response);
};

const cacheFirst = async (request) => {
    const responseFromCache = await caches.match(request);
    if (responseFromCache) {
        numberCall += 1
        console.log('response from cache', responseFromCache)
        responseFromCache.blob().then((blob) => {
            console.log('estimate size', blob.size)
            console.log('number call', numberCall)
            console.log('estimate total size', numberCall * blob.size)
        })
        return responseFromCache;
    }
    const responseFromNetwork = await fetch(request);

    putInCache(request, responseFromNetwork.clone());
    return responseFromNetwork;
};

self.addEventListener('fetch', (event) => {
    console.log(`[Service Worker] Fetched resource ${event.request.url}`);
    event.respondWith(cacheFirst(event.request));

});
